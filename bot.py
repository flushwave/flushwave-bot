import discord
import configparser
from discord.ext import commands
import asyncio
import logging

config = configparser.ConfigParser()
config.read('config.ini')
token = config['Client']['token']
rprefix = config['Client']['prefix']
prefix = rprefix.split('"')[1]

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

description = 'Flushwave is a bot solely created for assisting discord users.'

bot = commands.AutoShardedBot(command_prefix=commands.when_mentioned_or(prefix), description=description)

# this specifies what extensions to load when the bot starts up
startup_extensions = ['cogs.google', 'cogs.meta', 'cogs.poll', 'cogs.rng', 'cogs.api', 'cogs.mod', 'cogs.motd']

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

if __name__ == '__main__':
    for extension in startup_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            print('Failed to load extension {}\n{}'.format(extension, exc))

bot.run(token)
