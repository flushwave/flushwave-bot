from discord.ext import commands
import discord

class Moderation:
    '''Utilities for moderation'''

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def purge(self, ctx, amount: int):
        """
        Purge an amount of messages from a user/in a channel
        """
        if ctx.invoked_subcommand is None:
            g = await ctx.message.channel.purge(limit=amount)
            await ctx.send('Purged {} messages'.format(len(g)), delete_after=3.0)

    @commands.has_permissions(manage_messages=True)
    @purge.command()
    async def user(self, ctx, user: discord.Member, amount: int):
        """
        Purge an amount of messages from a user
        """
        def is_user(message):
            return message.author == user
        g = await ctx.message.channel.purge(limit=amount, check=is_user)
        await ctx.send('Purged {0} messages from {1}'.format(len(g), user.mention), delete_after=3.0)

    @commands.has_permissions(manage_nicknames=True)
    @commands.command(aliases=['nick'])
    async def nickname(self, ctx, *, name=None):
        """
        Set the nickname of Flushwave
        """
        await self.bot.change_nickname(ctx.guild.me, name)
        await ctx.send("\N{OK HAND SIGN}")

    @commands.has_permissions(kick_members=True)
    @commands.guild_only()
    @commands.command()
    async def kick(self, ctx, member: discord.Member):
        """
        Used to kick a member from a server
        """
        try:
            await ctx.guild.kick(member)
            await ctx.send("\N{OK HAND SIGN}")
        except discord.Forbidden:
            await ctx.send("Missing permissions")

    @commands.has_permissions(ban_members=True)
    @commands.guild_only()
    @commands.command()
    async def ban(self, ctx, member: discord.Member):
        """
        Used to ban a member from a server
        """
        try:
            await ctx.guild.ban(member)
            await ctx.send("\N{OK HAND SIGN}")
        except discord.Forbidden:
            await ctx.send("Missing permissions")

    @commands.has_permissions(ban_members=True)
    @commands.guild_only()
    @commands.command()
    async def unban(self, ctx, id: int):
        """
        Used to unban a member from a server
        """
        try:
            g = self.bot.get_user(id)
            await ctx.guild.unban(g)
            await ctx.send("\N{OK HAND SIGN}")
        except discord.Forbidden:
            await ctx.send("Missing permissions")

    @commands.has_permissions(manage_messages=True)
    @commands.command()
    async def clean(self, ctx, amount: int):
        """
        Clean the bot's messages
        """
        def is_me(ctx):
            return ctx.author == self.bot.user
        deleted = await ctx.channel.purge(limit=amount, check=is_me)
        await ctx.send('Deleted {} message(s)'.format(len(deleted)), delete_after=3.0)

def setup(bot):
    bot.add_cog(Moderation(bot))
