from discord.ext import commands
import random as rng

class RNG:
    '''Utilities that provide pseudo-RNG.'''

    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    async def random(self, ctx):
        '''Displays a random thing you request.'''
        if ctx.invoked_subcommand is None:
            await ctx.send(f'Incorrect random subcommand passed. Try {ctx.prefix}help random')

    @random.command()
    async def number(self, ctx, minimum=0, maximum=100):
        '''Displays a random number within an optional range.
        The minimum must be smaller than the maximum and the maximum number
        accepted is 1000.
        '''

        maximum = min(maximum, 1000)
        if minimum >= maximum:
            await ctx.send('Maximum is smaller than minimum.')
            return

        await ctx.send(rng.randint(minimum, maximum))

    @random.command()
    async def lenny(self, ctx):
        '''Displays a random lenny face.'''
        lenny = rng.choice([
            '( ͡° ͜ʖ ͡°)', '( ͠° ͟ʖ ͡°)', 'ᕦ( ͡° ͜ʖ ͡°)ᕤ', '( ͡~ ͜ʖ ͡°)',
            '( ͡o ͜ʖ ͡o)', '͡(° ͜ʖ ͡ -)', '( ͡͡ ° ͜ ʖ ͡ °)﻿', '(ง ͠° ͟ل͜ ͡°)ง',
            'ヽ༼ຈل͜ຈ༽ﾉ'
        ])
        await ctx.send(lenny)

    @commands.command()
    async def choose(self, ctx, *choices: commands.clean_content):
        '''Chooses between multiple choices.
        To denote multiple choices, you should use double quotes.
        '''
        if len(choices) < 2:
            return await ctx.send('Not enough choices to pick from.')

        await ctx.send(rng.choice(choices))

def setup(bot):
    bot.add_cog(RNG(bot))
