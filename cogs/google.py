import asyncio
from aiourlshortener import Shortener
from discord.ext import commands
import discord
import configparser
import datetime
from google import google

config = configparser.ConfigParser()
config.read('config.ini')
api_key = config['Google']['api-key']

class Google():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['goo.gl', 'googl', 'shorturl'])
    async def shorten(self, ctx, url: str):
        '''
        Shorten a url with goo.gl
        '''
        try:
            if '.' in url:
                shortener = Shortener('Google', api_key=api_key)
                short_url = await shortener.short(url)
                long_url = await shortener.expand(short_url)
                results = discord.Embed(title='Shortened URL', description=short_url, color=ctx.author.color)
                footer = results.set_footer(text='Long URL: {}'.format(long_url))
                await ctx.send(embed=results)
            else:
                await ctx.send('Invalid url. Are you including the tld?')

        except ValueError:
            await ctx.send('Invalid url. Are you putting https:// before the link?')


    @commands.command(aliases=['g', 'google'])
    async def search(self, ctx, query: str):
        '''
        Search something with google
        '''
        await ctx.trigger_typing()
        num_page = 3
        r = google.search(query, num_page)
        results = discord.Embed(title='Search Results', description='1. [{0.name}]({0.link}) - {0.description}\n\n2. [{1.name}]({1.link}) - {1.description}\n\n3. [{2.name}]({2.link}) - {2.description}'.format(r[0], r[1], r[2]), color=ctx.author.color)
        await ctx.send(embed=results)

def setup(bot):
    bot.add_cog(Google(bot))
