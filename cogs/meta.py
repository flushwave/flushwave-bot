import asyncio
from discord.ext import commands
import discord
import unicodedata
from collections import Counter
import traceback

class Meta():
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=['pong'])
    async def ping(self, ctx):
        '''
        Ping the bot.
        '''
        r = discord.Embed(title='Pong', description=str(round(self.bot.latency*1000)) + 'ms', color=ctx.author.color)
        await ctx.send(embed=r)

    @commands.is_owner()
    @commands.command(aliases=['eval'], hidden=True)
    async def evaluate(self, ctx, *, input: str):
        '''
        Evaluate code.
        '''
        try:
            s = eval(input)
        except:
            s = traceback.format_exc()

        r = discord.Embed(title='Evaluation Results:', description='Input: ```py\n{0}```\nOutput:\n```py\n{1}```'.format(input, s))
        await ctx.send(embed=r)

    @commands.is_owner()
    @commands.command(aliases=['exec'], hidden=True)
    async def execute(self, ctx, *, input: str):
        '''
        Execute code.
        '''
        try:
            s = exec(input)
        except:
            s = traceback.format_exc()
            r = discord.Embed(title='Evaluation Results:', description='Input: ```py\n{0}```\nOutput:\n```py\n{1}```'.format(input, s))
            await ctx.send(embed=r)

    @commands.is_owner()
    @commands.command(hidden=True)
    async def quit(self, ctx):
        '''
        Quits the bot.
        '''
        await self.bot.logout()

    @commands.is_owner()
    @commands.command(rest_is_raw=True, hidden=True)
    async def echo(self, ctx, *, content):
        await ctx.send(content)

    @commands.command(aliases=['join'])
    async def invite(self, ctx):
        '''Get bot invite.'''
        perms = discord.Permissions.none()
        perms.administrator = True
        e = discord.Embed(title='Bot Invite', description=f'<{discord.utils.oauth_url(self.bot.user.id, perms)}>', color=ctx.author.color)
        await ctx.send(embed=e)

    @commands.command()
    async def charinfo(self, ctx, *, characters: str):
        '''Shows you information about a number of characters.
        Only up to 25 characters at a time.
        '''

        if len(characters) > 25:
            return await ctx.send(f'Too many characters ({len(characters)}/25)')

        def to_string(c):
            digit = f'{ord(c):x}'
            name = unicodedata.name(c, 'Name not found.')
            return f'`\\U{digit:>08}`: {name} - {c} \N{EM DASH} <http://www.fileformat.info/info/unicode/char/{digit}>'

        await ctx.send('\n'.join(map(to_string, characters)))

    @commands.guild_only()
    @commands.command(aliases=['uinfo', 'user_info'])
    async def userinfo(self, ctx, *, member: discord.Member = None):
        '''Shows info about a member.
        This cannot be used in private messages. If you don't specify
        a member then the info returned will be yours.
        '''

        if member is None:
            member = ctx.author

        e = discord.Embed()
        roles = [role.name.replace('@', '@\u200b') for role in member.roles]
        shared = sum(1 for m in self.bot.get_all_members() if m.id == member.id)
        voice = member.voice
        if voice is not None:
            vc = voice.channel
            other_people = len(vc.members) - 1
            voice = f'{vc.name} with {other_people} others' if other_people else f'{vc.name} by themselves'
        else:
            voice = 'Not connected.'

        e.set_author(name=str(member))
        e.set_footer(text='Member since').timestamp = member.joined_at
        e.add_field(name='ID', value=member.id)
        e.add_field(name='Servers', value=f'{shared} shared')
        e.add_field(name='Created', value=member.created_at)
        e.add_field(name='Voice', value=voice)
        e.add_field(name='Roles', value=', '.join(roles) if len(roles) < 10 else f'{len(roles)} roles')
        e.color = member.color

        if member.avatar:
            e.set_thumbnail(url=member.avatar_url)

        await ctx.send(embed=e)

    @commands.is_owner()
    @commands.command(hidden=True)
    async def game(self, ctx, *, game):
        '''
        Set the bot's playing status.
        '''
        await self.bot.change_presence(game=discord.Game(name=game))
        await ctx.send('<:thumbsup:350643674587201548>')

def setup(bot):
    bot.add_cog(Meta(bot))
