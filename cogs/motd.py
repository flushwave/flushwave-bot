from discord.ext import commands
import discord
import pickle
import datetime
from cogs.util import motd_out as mout

class MoTD:
    '''Utilities related to the Message Of The Day'''

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def motd(self, ctx):
        pass

    @motd.command()
    async def show(self, ctx):
        picklemotd = open('data/motd.pickle', 'rb')
        mesgotd = pickle.load(picklemotd)
        e = discord.Embed(title='Message Of The Day', description=mesgotd[str(datetime.date.today())], color=ctx.author.color)
        await ctx.send(embed=e)

    @motd.command()
    async def date(self, ctx, *, date):
        picklemotd = open('data/motd.pickle', 'rb')
        mesgotd = pickle.load(picklemotd)
        e = discord.Embed(title='Message Of {}'.format(date), description=mesgotd[str(date)], color=ctx.author.color)
        await ctx.send(embed=e)

    @commands.is_owner()
    @motd.command()
    async def add(self, ctx, *, input: str):
        await mout.output(datetime.date.today(), input)
        await ctx.send('Done')

    @commands.is_owner()
    @motd.command()
    async def addcustom(self, ctx, key: str, *, input: str):
        await mout.output(key, input)
        await ctx.send('Done')

    @motd.command()
    async def list(self, ctx):
        picklemotd = open('data/motd.pickle', 'rb')
        mesgotd = pickle.load(picklemotd)
        e = discord.Embed(title='MoTD list', description=str(mesgotd), color=ctx.author.color)
        await ctx.send(embed=e)

def setup(bot):
    bot.add_cog(MoTD(bot))
