import pickle
import datetime

async def output(key, input):
    date = str(key)
    motd_dict = {date: str(input)}

    pickle_out = open('data/motd.pickle', 'wb')
    pickle.dump(motd_dict, pickle_out)
