import traceback
import sys
import discord.ext

'''This example uses cogs! If you are not already using cogs please see:
https://gist.github.com/leovoel/46cd89ed6a8f41fd09c5
By using cogs we can keep things clean, and easier to read/add/edit later. 
You can also use this module for custom Exception Classes. But for this example we will keep things simple.
This example uses @rewrite version of the lib. For the async verion of the lib, simply swap the places of ctx, and error.
e.g: on_command_error(self, error, ctx)
For a list of exceptions:
http://discordpy.readthedocs.io/en/rewrite/ext/commands/api.html#errors'''


class CommandErrorHandler:
    def __init__(self, bot):
        self.bot = bot

    async def on_command_error(self, ctx, error):
        '''The event triggered when an error is raised while invoking a command.
        ctx   : Context
        error : Exception'''

        # Check if our error is an instance of CommandNotFound
        if isinstance(error, discord.ext.commands.CommandNotFound):
            return  # You could do anything you like here... 
            # But I just want to ignore this error and stop it printing anything.
        
        # Here we check if the bot has failed to run the command due to not having the required permissions.
        try:
            if isinstance(error.original, discord.errors.Forbidden):
                await ctx.send('**I do not have the required permissions to run this command.**')
        except AttributeError:
            pass

        # If True, we will notify this person that they are missing an argument
        if isinstance(error, discord.ext.commands.MissingRequiredArgument):
            try:
                await ctx.send('Missing a required argument. Use >>>help <command> to get help on a specific command')
            except:
                return

        # If True, we will notify this person that they have a bad argument
        if isinstance(error, discord.ext.commands.BadArgument):
            try:
                await ctx.send('Bad argument. Use {0.command_prefix}help <command> to get help on a specific command'.format(self.bot))
            except:
                return

def setup(bot):
    bot.add_cog(CommandErrorHandler(bot))

