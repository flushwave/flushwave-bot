from discord.ext import commands
import aiohttp
import discord
import hastebin

class API:
    '''Things to do with APIs.'''

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.command()
    async def cat(self, ctx):
        '''Gets a random cat.'''
        with aiohttp.ClientSession() as session:
            async with session.get('http://random.cat/meow') as r:
                if r.status == 200:
                    js = await r.json()
                    l = discord.Embed(title='Have a cat!', color=ctx.author.color)
                    l.set_image(url=js['file'])
                    await ctx.send(embed=l)

    @commands.command()
    async def dog(self, ctx):
        '''Gets a random dog.'''
        with aiohttp.ClientSession() as session:
            async with session.get('http://random.dog/woof.json') as r:
                if r.status == 200:
                    js = await r.json()
                    l = discord.Embed(title='Have a dog!', color=ctx.author.color)
                    l.set_image(url=js['url'])
                    await ctx.send(embed=l)

    @commands.command()
    async def pat(self, ctx, user: discord.User):
        '''Pat someone.'''
        with aiohttp.ClientSession() as session:
            async with session.get('https://rra.ram.moe/i/r?type=pat') as r:
                if r.status == 200:
                    js = await r.json()
                    e = discord.Embed(description='{0}, you got a pat from {1}'.format(user.name, ctx.author.name), color=ctx.author.color)
                    e.set_footer(text='Powered by Wolke\'s API')
                    e.set_image(url='https://rra.ram.moe{}'.format(js['path']))
                    await ctx.send(embed=e)

    @commands.command()
    async def haste(self, ctx, lang: str, *, content: str):
        '''Upload something to hastebin.'''
        g = discord.Embed(title='Hastebin URL', description='Input: ```{0}\n{1}\n```\nURL: {2}'.format(lang, content, hastebin.post(content), color=ctx.author.color))
        await ctx.send(embed=g)

def setup(bot):
    bot.add_cog(API(bot))

